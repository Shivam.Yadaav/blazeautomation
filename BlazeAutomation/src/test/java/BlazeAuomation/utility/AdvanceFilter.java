package BlazeAuomation.utility;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.paulhammant.ngwebdriver.ByAngular;

import BlazeAutomation.configuration.WaitTime;

public class AdvanceFilter {
	
	
	
	public static void createFilter(WebDriver driver ,String columnName) {
		WebElement advFilterBtn=Locators.waitTillElement(driver, By.xpath("//span[@role='button'][normalize-space()='All Accounts']"), WaitTime.waitElement);
		advFilterBtn.click();
		WebElement nameFilter=Locators.waitTillElement(driver, By.id("filterHeaderTitle"), 10);
		nameFilter.isEnabled();
		Actions dblClick= new Actions(driver);
		dblClick.moveToElement(nameFilter).doubleClick(nameFilter).sendKeys(nameFilter,"Filter"+Timestamp.getTimestamp()).build().perform();
	
		
		WebElement selectColumn=Locators.waitTillElement(driver, ByAngular.model("$mdAutocompleteCtrl.scope.searchText"), 10);
		selectColumn.isDisplayed();
		selectColumn.click();
		selectColumn.sendKeys(columnName);
		DropdownHandling.select(driver, By.xpath("//li[@md-virtual-repeat='item in $mdAutocompleteCtrl.matches']"), columnName);
		Locators.waitTillElement(driver, ByAngular.model("colExp.Values.Value1"), WaitTime.waitElement).sendKeys("alonso");
		
		Locators.waitTillElement(driver, By.xpath("//span[normalize-space()='SAVE AS']"), 10).click();
}
    public static String applyFilter(WebDriver driver,String filterName) {
    	WebElement advFilterBtn=Locators.waitTillElement(driver, By.xpath("//span[normalize-space()='MY FILTERS']"), WaitTime.waitElement);
		advFilterBtn.click();
        List <WebElement> elmList=Locators.waitTillElements(driver, By.xpath("//div[contains(@class,'data-grid-cell ui-grid-cell-contents')][normalize-space()='Cash Outside MinMax Range']"), 20);
        if(elmList.isEmpty()) {
        	Locators.waitTillElement(driver, By.xpath("//md-tab-item[normalize-space()='SHARED WITH ME']"), 10).click();
        	 List <WebElement> elList=Locators.waitTillElements(driver, By.xpath("//div[contains(@class,'data-grid-cell ui-grid-cell-contents')][normalize-space()='Cash Outside MinMax Range']"), 20);
        	 String d= elList.get(0).getText();
         	elList.get(0).click();
         	return d;
        }
        else {
        	String f= elmList.get(0).getText();
        	elmList.get(0).click();
        	 Locators.waitTillElement(driver, By.xpath("//span[normalize-space()='APPLY']"), 10).click();
        	 return f; 
        }
		
        
    
 }
}
