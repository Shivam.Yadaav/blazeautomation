package BlazeAuomation.utility;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.paulhammant.ngwebdriver.ByAngular;

import BlazeAutomation.configuration.WaitTime;

public class QuickSearch {
	
	public static void searchPortfolio(WebDriver driver, String accountShortName) {
		QuickSearch.clickSearch(driver).click();
		WebElement searchBox=Locators.waitTillElement(driver, By.id("header-search"), WaitTime.waitElement);
		searchBox.click();
		searchBox.sendKeys(accountShortName);
		DropdownHandling.select(driver, ByAngular.repeater("portfolio in headerModel.searchList.Portfolios"),accountShortName);
		Locators.waitTillElement(driver, By.id("portfolioDescription"), 20);
		}
	public static void searchHousehold(WebDriver driver,String householdID) {
		QuickSearch.clickSearch(driver).click();
		WebElement searchBox=Locators.waitTillElement(driver, By.id("header-search"), WaitTime.waitElement);
		searchBox.click();
		searchBox.sendKeys(householdID);
		DropdownHandling.select(driver, ByAngular.repeater("household in headerModel.searchList.HouseHolds"),householdID);
		Locators.waitTillElement(driver, By.id("householdId"), WaitTime.waitElement);
	}
	public static void searchGroup(WebDriver driver,String groupID) {
		QuickSearch.clickSearch(driver).click();
		WebElement searchBox=Locators.waitTillElement(driver, By.id("header-search"), WaitTime.waitElement);
		searchBox.click();
		searchBox.sendKeys(groupID);
		DropdownHandling.select(driver, ByAngular.repeater("group in headerModel.searchList.Groups"),groupID);
		Locators.waitTillElement(driver, By.id("groupId"), WaitTime.waitElement);
	}
	public static void applyFilter(WebDriver driver,String globalFilter) {
		QuickSearch.clickSearch(driver).click();
		WebElement searchBox=Locators.waitTillElement(driver, By.id("header-search"), WaitTime.waitElement);
		searchBox.click();
		searchBox.sendKeys(globalFilter);
		DropdownHandling.select(driver, ByAngular.repeater("searchFilter in headerModel.searchList.Filters"),globalFilter);
	}
	
	private static WebElement clickSearch(WebDriver driver) {
		WebElement searchOpt=Locators.waitTillElement(driver, By.xpath("//em[@class='fa fa-search header-icons']"),WaitTime.waitElement);
		return searchOpt;
		}
}
