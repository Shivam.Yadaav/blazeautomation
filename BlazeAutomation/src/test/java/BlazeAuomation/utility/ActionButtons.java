package BlazeAuomation.utility;

import java.awt.List;
import java.util.Collection;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import BlazeAutomation.configuration.WaitTime;

public class ActionButtons {
	public static void action(WebDriver driver,String action) {
		java.util.List<WebElement> actionBtns=Locators.waitTillElements(driver,By.className("common-action-button-text"), WaitTime.waitElement);
		for(WebElement actionBtn: actionBtns ) {
			System.out.println(actionBtn.getText());
			if(actionBtn.getText().toLowerCase().contains(action)) {
				Actions click=new Actions(driver);
				click.moveToElement(actionBtn).click(actionBtn).build().perform();
				break;
			}
		}
	}

}
