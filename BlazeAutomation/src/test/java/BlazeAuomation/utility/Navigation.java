package BlazeAuomation.utility;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.paulhammant.ngwebdriver.ByAngular;

import BlazeAutomation.configuration.WaitTime;

public class Navigation {

	public static void funNavigation(String page, WebDriver driver) {
		if (page.equals("Accounts") || page.equals("Households") || page.equals("Groups")) {
			WebElement dd = Locators
					.waitTillElementsAngular(driver, ByAngular.repeater("list in headerModel.headerList"), 20).get(1);
			Actions ac = new Actions(driver);
			ac.moveToElement(dd).build().perform();
			DropdownHandling.select(driver, ByAngular.repeater("sublist in list.DropList"), page);
			Locators.waitTillElementAngular(driver, ByAngular.repeater("col in colContainer.renderedColumns track by col.colDef.name"), WaitTime.waitElement);
		}

		else if (page.contains("Pending") || page.contains("Rebalance")) {
			WebElement dd = Locators.waitTillElement(driver, By.id("ORDERAPPROVE-navName"), WaitTime.waitElement);
			Actions ac = new Actions(driver);
			ac.moveToElement(dd).build().perform();
			DropdownHandling.select(driver, ByAngular.repeater("sublist in list.DropList"), page);
		} else if (page.contains("Trading") || page.contains("Trade")) {
			WebElement dd = Locators.waitTillElement(driver, By.id("TRADING-navName"), WaitTime.waitElement);
			Actions ac = new Actions(driver);
			ac.moveToElement(dd).build().perform();
			DropdownHandling.select(driver, ByAngular.repeater("sublist in list.DropList"), page);

		}

		else if (page.contains("Models") || page.contains("Drift")) {
			WebElement dd = Locators.waitTillElement(driver, By.id("MODELS-navName"), WaitTime.waitElement);
			Actions ac = new Actions(driver);
			ac.moveToElement(dd).build().perform();
			DropdownHandling.select(driver, ByAngular.repeater("sublist in list.DropList"), page);

		}

		else if (page.contains("Security") || page.contains("Asset")) {
			WebElement dd = Locators.waitTillElement(driver, By.id("SECURITIES-navName"), WaitTime.waitElement);
			Actions ac = new Actions(driver);
			ac.moveToElement(dd).build().perform();
			DropdownHandling.select(driver, ByAngular.repeater("sublist in list.DropList"), page);
		} else if (page.contains("Import")) {
			WebElement dd = Locators.waitTillElement(driver, By.id("IMPORT-navName"), WaitTime.waitElement);
			Actions ac = new Actions(driver);
			ac.moveToElement(dd).build().perform();
			List<WebElement> eee = driver.findElements(ByAngular.repeater("sublist in list.DropList"));
			for (WebElement ty : eee) {
				if (ty.getText().contains(page)) {
					ty.click();
					System.out.println(ty.getText());
				}

			}

		} else {
			WebElement dd = Locators.waitTillElement(driver, By.xpath("//*[@id='myNavbar']/ul[1]/li[8]/a[1]"),
					WaitTime.waitElement);

			Actions ac = new Actions(driver);
			ac.moveToElement(dd).build().perform();
			DropdownHandling.select(driver, ByAngular.repeater("sublist in list.DropList"), page);

		}
	}
}

//public static void funNavigation(String page, WebDriver driver){
//	if(page.equals("Accounts")||page.equals("Households")||page.equals("Groups")) {
//	WebElement dd=Locators.waitTillElement(driver, By.id("PORTFOLIOS-navName"),20);
//	Actions ac= new Actions(driver);
//	ac.moveToElement(dd).build().perform();
//	List <WebElement> eee=driver.findElements(ByAngular.repeater("sublist in list.DropList"));
//	for(WebElement ty:eee) {
//		 if(ty.getText().contains(page)) {
//			  ty.click();
//			  System.out.println(ty.getText());
//		  }
//	}
//	}
//	
//	else if(page.contains("Pending") ||page.contains("Rebalance")) {
//		WebElement dd=Locators.waitTillElement(driver, By.id("ORDERAPPROVE-navName"), 20);
//	  Actions ac= new Actions(driver);
//	  ac.moveToElement(dd).build().perform();
//	  List <WebElement> eee=driver.findElements(ByAngular.repeater("sublist in list.DropList"));
//	  for(WebElement ty:eee) {
//		  if(ty.getText().contains(page)) {
//			  ty.click();
//			  System.out.println(ty.getText());
//		  }
//	      
//		}
//		
//	}
//	else if(page.contains("Trading") ||page.contains("Trade")) {
//		WebElement dd=Locators.waitTillElement(driver, By.id("TRADING-navName"), 20);
//	  Actions ac= new Actions(driver);
//	  ac.moveToElement(dd).build().perform();
//	  List <WebElement> eee=driver.findElements(ByAngular.repeater("sublist in list.DropList"));
//	  for(WebElement ty:eee) {
//		  if(ty.getText().contains(page)) {
//			  ty.click();
//			  System.out.println(ty.getText());
//		  }
//	      
//		}
//		
//	}
//	
//	else if(page.contains("Models") ||page.contains("Drift")) {
//		WebElement dd=Locators.waitTillElement(driver, By.id("MODELS-navName"), 20);
//	  Actions ac= new Actions(driver);
//	  ac.moveToElement(dd).build().perform();
//	  List <WebElement> eee=driver.findElements(ByAngular.repeater("sublist in list.DropList"));
//	  for(WebElement ty:eee) {
//		  if(ty.getText().contains(page)) {
//			  ty.click();
//			  System.out.println(ty.getText());
//		  }
//	      
//		}
//		
//	}
//	
//	else if(page.contains("Security") ||page.contains("Asset")) {
//		WebElement dd=Locators.waitTillElement(driver, By.id("SECURITIES-navName"), 20);
//	  Actions ac= new Actions(driver);
//	  ac.moveToElement(dd).build().perform();
//	  List <WebElement> eee=driver.findElements(ByAngular.repeater("sublist in list.DropList"));
//	  for(WebElement ty:eee) {
//		  if(ty.getText().contains(page)) {
//			  ty.click();
//			  System.out.println(ty.getText());
//		  }
//	      
//		}
//		
//	}
//	else if(page.contains("Import")) {
//		WebElement dd=Locators.waitTillElement(driver, By.id("IMPORT-navName"), 20);
//	  Actions ac= new Actions(driver);
//	  ac.moveToElement(dd).build().perform();
//	  List <WebElement> eee=driver.findElements(ByAngular.repeater("sublist in list.DropList"));
//	  for(WebElement ty:eee) {
//		  if(ty.getText().contains(page)) {
//			  ty.click();
//			  System.out.println(ty.getText());
//		  }
//	      
//		}
//		
//	}
//	else{
//		WebElement dd=Locators.waitTillElement(driver,By.xpath("//*[@id='myNavbar']/ul[1]/li[8]/a[1]"), 20);
//	
//	  Actions ac= new Actions(driver);
//	  ac.moveToElement(dd).build().perform();
//	  DropdownHandling.select(driver,ByAngular.repeater("sublist in list.DropList"), 20, page);
//
//		
//	}
//}}
