package BlazeAuomation.utility;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import BlazeAutomation.configuration.WaitTime;

public class DropdownHandling {

	public static void select(WebDriver driver, By locator, String p) {
		JavascriptExecutor js = (JavascriptExecutor) driver;

		List<WebElement> drop = Locators.waitTillElementsAngular(driver, locator, WaitTime.dropdownSelect);

		for (WebElement elemdrop : drop) {
			System.out.println(elemdrop.getText());
			if (elemdrop.getText().contains(p)) {
				js.executeScript("arguments[0].scrollIntoView();", elemdrop);
				Actions hdh = new Actions(driver);
				hdh.moveToElement(elemdrop).click().build().perform();
				break;

			}

		}

	}
}