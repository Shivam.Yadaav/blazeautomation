package BlazeAuomation.utility;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Locators {
   private static WebDriverWait wait;

	public static WebElement waitTillElement(WebDriver driver, By locator, int time)
	{
		wait =new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return driver.findElement(locator);
		
	}
	public static WebElement waitTillElementAngular(WebDriver driver, By locator, int time)
	{
		 wait =new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return driver.findElement(locator);
		
	}

	public static List <WebElement> waitTillElements(WebDriver driver, By locator, int time)
	{
		 wait =new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return driver.findElements(locator);
		
	}
	public static  List<WebElement> waitTillElementsAngular(WebDriver driver, By locator, int time)
	{
		 wait =new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return driver.findElements(locator);
		
	}
	public static WebDriverWait getWait(WebDriver driver, int time) {
		wait =new WebDriverWait(driver, time);
		return wait;
	}

}
