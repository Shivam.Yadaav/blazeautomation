package BlazeAuomation.utility;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import BlazeAutomation.configuration.WaitTime;

public class GridUtil {

	
	public static List<WebElement> griddata( WebDriver driver,By locator, int time){
		
		
		List<WebElement>datalist=Locators.waitTillElements(driver, locator, WaitTime.pageLoading);
		return datalist;
		
	}
	public static void viewRecord(List<WebElement> gridDataElement,WebDriver driver,String name) {
		for (WebElement selectElement : gridDataElement) {
			if (selectElement.getText().contains(name)) {
				Actions cllickGrid = new Actions(driver);
				cllickGrid.moveToElement(selectElement).click(selectElement).build().perform();
				break;
			}
		} 
		Locators.waitTillElement(driver, By.id("prAcBt_PortfolioList_View"), 20).click();
		}
	public static void selectPopupRecord(List<WebElement> eslist, WebDriver driver, String record) {
		for (WebElement sel : eslist) {
			if (sel.getText().equals(record)) {
				Actions cl = new Actions(driver);
				cl.doubleClick(sel).build().perform();
				break;
			}
		}
	}
}
