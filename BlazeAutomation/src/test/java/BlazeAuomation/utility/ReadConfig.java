package BlazeAuomation.utility;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {

	Properties pro;
	public ReadConfig() {
		File src= new File("./Configuration/Config.properties");
		try {
			FileInputStream fis= new FileInputStream(src);
			pro=new Properties();
			pro.load(fis);
		} catch (Exception e) {
			System.out.println("Exception is"+ e.getMessage());
		}
	}

	public String getBrowser() {

		String Browser=pro.getProperty("Browser");
		return Browser;

	}

	public Boolean setHeadlessmode() {
		if (pro.getProperty("HeadlessMode").equals("True"))
		{
			return true;
		} 
		else {
			return false;
		}

	}

	public String getUrl() {

		String url= pro.getProperty("Url");
		return url;
	}

	public String getUsername() {

		String username= pro.getProperty("Username");
		return username;
	}

	public String getPassword() {

		String password=pro.getProperty("Password");
		return password;
	}

}
