package BlazeAuomation.utility;

import java.time.LocalTime;

public class Timestamp {
	
	public static String getTimestamp() {
		LocalTime str=java.time.LocalTime.now();
		String time=str.toString();
		String replaceTime=time.replace(":", "");
		String Timestamp=replaceTime.replace(".", "");
		return Timestamp;
	}

}
