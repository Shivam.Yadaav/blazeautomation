package BlazeAuomation.utility;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.paulhammant.ngwebdriver.ByAngular;

public class QuickFilter {
	
	public static void applyQuickFilter(String data,WebDriver driver,Boolean f) {
		//WebElement l=Locators.waitTillElement(driver,By.xpath("//input[@placeholder='QUICK FILTER']") , 10);
		if (f==false) {
			WebElement gg=Locators.waitTillElementAngular(driver, ByAngular.model("dataGridModel.searchVal"), 20);
			gg.click();
					gg.sendKeys(data);
			gg.sendKeys(Keys.RETURN);
			
		} else if (f==true) {
			
			WebElement gvg=Locators.waitTillElement(driver, By.xpath("//input[@class='form-control data-grid-filter-search-input ng-pristine ng-valid ng-empty ng-touched']"), 20);
			gvg.click();
			gvg.sendKeys(data);
	gvg.sendKeys(Keys.RETURN);
		
	}

	}}
