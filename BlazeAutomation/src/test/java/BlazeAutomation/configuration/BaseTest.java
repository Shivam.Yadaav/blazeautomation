package BlazeAutomation.configuration;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import BlazeAuomation.utility.Locators;

public class BaseTest extends Setup {




	//@BeforeClass
	@BeforeMethod
	public static void login() {
		setupEnv();
		try {
			Locators.waitTillElement(driver, By.id("login-id"), 20).sendKeys(ReadConfig.getUsername());
			Locators.waitTillElement(driver, By.id("login-password"), 10).sendKeys(ReadConfig.getPassword());
			Locators.waitTillElement(driver, By.id("login-submit"), 10).click();
            Thread.sleep(5000);
            
			if (!driver.getTitle().contains("Dashboard")) {
			    WebElement loginMessage= Locators.getWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='loginMessageDiv']")));
				if (!loginMessage.getText().isEmpty()) {
				 WebElement gg=Locators.waitTillElement(driver, By.cssSelector("#loginMessageDiv"), 10);
					logger.info(gg.getText());
				} 
				else {
					String logMsg = Locators.waitTillElement(driver, By.xpath(
							"//div[@class='popup-body common-popup-content atomPopupContentMinHeight ng-binding']"), 10)
							.getText();
					logger.info(logMsg);
					Locators.waitTillElement(driver, By.xpath("//*[@id='popAcBt_Confirm_Yes']"), 10).click();
					logger.info("Logged In User" + ReadConfig.getUsername());
				}
			} 
			else {
				logger.info("Logged In User" + ReadConfig.getUsername());
				}
		}
		catch (Exception e) {
			logger.info(e);
		}
	}
	
	


	//@AfterClass
	@AfterMethod
	public static  void logout(){
		Locators.waitTillElement(driver,By.linkText(ReadConfig.getUsername()), 20).click();
		Locators.waitTillElement(driver,By.partialLinkText("Log"), 20).click();
		String actual=Locators.waitTillElement(driver, By.linkText("Forgot Password?"), 5).getText();
		Assert.assertEquals("Forgot Password?",actual);
		logger.info("Logged Out Successfully ");
		driver.quit();
		logger.info("Browser closed" );
	}
}

