package BlazeAutomation.configuration;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class Config {

	private static String DRIVER_DIR = System.getProperty("user.dir") 
			+ File.separator + "Drivers" + File.separator;
	private static String CHROME_DRIVER_PATH = DRIVER_DIR + "chromedriver";  
	private static String GECKO_DRIVER_PATH = DRIVER_DIR + "geckodriver";
	private String SCREENSHOT_DIR = System.getProperty("user.dir")
			+ File.separator
			+ "Screenshots"
			+ File.separator;

	//Mac or windows(add .exe to path for windows)//come 
	private static String modifyIfWindows(String inPath) {
		if (System.getProperty("os.name").toLowerCase().contains("windows"))
		{
			return inPath + ".exe";
		} 
		else if(System.getProperty("os.name").toLowerCase().contains("OS"));
		{
			return inPath;
		}
	}
//Make different class
	//Method overloading to initiate driver
	public static WebDriver createChromeDriver(ChromeOptions options) {
		System.setProperty("webdriver.chrome.driver", modifyIfWindows(CHROME_DRIVER_PATH));
		return new ChromeDriver(options);
	}

	public static WebDriver createChromeDriver() {
		return createChromeDriver(new ChromeOptions());
	}

	public static WebDriver createFireFoxDriver(FirefoxOptions options) {
		System.setProperty("webdriver.gecko.driver", modifyIfWindows(GECKO_DRIVER_PATH));
		return new FirefoxDriver(options);
	}

	public WebDriver createFireFoxDriver() {
		return createFireFoxDriver(new FirefoxOptions());
	}

}
