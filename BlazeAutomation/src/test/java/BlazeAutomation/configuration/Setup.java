package BlazeAutomation.configuration;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;

import com.paulhammant.ngwebdriver.NgWebDriver;

import BlazeAuomation.utility.ReadConfig;

public class Setup {
		public static WebDriver driver;
		public static NgWebDriver ng;
		public static ReadConfig ReadConfig;//remove this use extend class
		public static Logger logger;
	
	public static void setupEnv() {
        
		ng=new NgWebDriver((JavascriptExecutor) driver);
		ReadConfig = new ReadConfig();
		logger = Logger.getLogger("logs");
		PropertyConfigurator.configure("Log4j.properties");

		if (ReadConfig.getBrowser().equals("chrome")) 
		{
			ChromeOptions options = new ChromeOptions();
			options.setHeadless(ReadConfig.setHeadlessmode());
			 driver=(Config.createChromeDriver(options));
			logger.info("Browser Launched");
		} 
		else if (ReadConfig.getBrowser().equals("firefox")) 
		{
			FirefoxOptions options = new FirefoxOptions();
			options.setHeadless(ReadConfig.setHeadlessmode());//Headless mode True or False
			driver=(Config.createFireFoxDriver(options));
			logger.info("Firefox driver started");
		}

		driver.get(ReadConfig.getUrl());
		driver.manage().window().maximize();
	}
	
	

}
