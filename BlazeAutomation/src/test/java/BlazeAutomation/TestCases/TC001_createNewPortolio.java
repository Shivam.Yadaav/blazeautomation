package BlazeAutomation.TestCases;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.paulhammant.ngwebdriver.ByAngular;

import BlazeAuomation.utility.Locators;
import BlazeAuomation.utility.Navigation;
import BlazeAuomation.utility.QuickFilter;
import BlazeAuomation.utility.QuickSearch;
import BlazeAuomation.utility.Timestamp;
import BlazeAuomation.utility.ActionButtons;
import BlazeAuomation.utility.DropdownHandling;
import BlazeAuomation.utility.GridUtil;
import BlazeAutomation.configuration.BaseTest;

public class TC001_createNewPortolio extends BaseTest {
  @Test
  public void f(){
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    String name = "Auto"+Timestamp.getTimestamp();
	    
		// Step 1:Go to Portfolio listing page
		Navigation.funNavigation("Accounts", driver);
		// Step 2:Click on Create button
		ActionButtons.action(driver, "create");

		// Step 3:Click on Shortname field and enter Shortname
		Locators.waitTillElement(driver, By.id("portfolioDescription"), 20).sendKeys(name);

		// Step 4:Click on description field and enter name
		Locators.waitTillElement(driver, By.id("portfolioShortName"), 20).sendKeys(name);
		
		// Step 5:Select Manager from dropdown	
		WebElement managerdropdown = Locators.waitTillElement(driver, By.id("managerSelect"), 20);
		Actions click = new Actions(driver);
		click.moveToElement(managerdropdown).click(managerdropdown).build().perform();
		 DropdownHandling.select(driver, ByAngular.repeater("item in portfolioDetailsModel.managerDropdown | filter: portfolioDetailsModel.managerSearch"), "F.Lampard");

		//Step 6:Click on save button
		//Locators.waitTillElement(driver, By.xpath("//*[@id='prAcBt_Portfolio_Save']/div[1]"), 20).click();
		 ActionButtons.action(driver, "save");
		


		//Step 7:Add broker
		WebElement brokerElement = Locators.waitTillElement(driver, By.id("acBt_BrokerAccount_Create"), 20);
		js.executeScript("arguments[0].scrollIntoView();", brokerElement);
		brokerElement.click();
		Locators.waitTillElement(driver, By.id("brokerId"), 20).click();
		
		DropdownHandling.select(driver, ByAngular.exactRepeater("item in brokerList"), "Schwab");
		Locators.waitTillElement(driver, By.id("accountNumber"), 20).sendKeys("123");
		Locators.waitTillElement(driver, By.id("acBt_BrokerAccountDetail_Submit"), 20).click();

		//Step 8:Add cash
		Locators.waitTillElement(driver, By.xpath("//li[@id='prAcBt_Portfolio_Cash_Wd']//div[1]"), 20).click();
		Locators.waitTillElement(driver, By.id("Amount_1"), 20).sendKeys("10000");
		
		WebElement cashType=Locators.waitTillElement(driver, By.xpath("/html[1]/body[1]/cash-wd-details-popup[1]/div[1]/div[1]/form[1]/div[1]/div[2]/md-select[1]/md-select-value[1]/span[1]"), 20);
		Actions clickCashType = new Actions(driver);
		clickCashType.moveToElement(cashType).click(cashType).build().perform();
		driver.findElement(ByAngular.repeater("(key,value) in cashFlowList").row(0)).click();
		
		Locators.waitTillElement(driver, By.id("acBt_CashWdDetail_Submit"), 20).click();
		

		//Step 9:Verify portfolio
		Navigation.funNavigation("Accounts", driver);
		QuickSearch.searchPortfolio(driver, name);
		Locators.waitTillElement(driver, By.id("portfolioDescription"), 10);
		String actualName=Locators.waitTillElement(driver, By.xpath("//h2[contains(text(),name)]"), 20).getText().toString();
		System.out.println(actualName);
		Assert.assertTrue(actualName.contains(name),"Portfolio successfully created");
	
		
		
  }
}



//public void f() throws Exception {
//	  String name = "Auto76";
//		// Step 1:Go to Portfolio listing page
//		//driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
//		Navigation navigate = new Navigation();
//		navigate.funNavigation("Accounts", driver);
//
//		// Step 2:Click on Create button
//		Locators.waitTillElement(driver, By.xpath("//em[@class='fal fa-plus-circle common-action-button-icon']"), 20)
//		.click();
//
//		// Step 3:Click on Shortname field and enter Shortname
//		Locators.waitTillElement(driver, By.id("portfolioDescription"), 20).sendKeys(name);
//
//		// Step 4:Click on description field and enter name
//		Locators.waitTillElement(driver, By.id("portfolioShortName"), 20).sendKeys(name);
//		
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//	
//		// Step 5:Select Manager from dropdown	
//		WebElement managerdropdown = Locators.waitTillElement(driver, By.id("managerSelect"), 0);
//		Actions click = new Actions(driver);
//		click.moveToElement(managerdropdown).click(managerdropdown).build().perform();
//		List<WebElement> managerList = driver.findElements(ByAngular.repeater(
//				"item in portfolioDetailsModel.managerDropdown | filter: portfolioDetailsModel.managerSearch"));
//		for (WebElement managerElement : managerList) {
//			System.out.println(managerElement.getText());
//			if (managerElement.getText().contains("F.Lampard")) {
//				Actions selectmanager = new Actions(driver);
//				js.executeScript("arguments[0].scrollIntoView();", managerElement);
//				selectmanager.moveToElement(managerElement).click().build().perform();
//
//			}
//		}
//
//		//Step 6:Click on save button
//		Locators.waitTillElement(driver, By.xpath("//*[@id='prAcBt_Portfolio_Save']/div[1]"), 20).click();
//		Thread.sleep(5000);
//
//
//		//Step 7:Add broker
//		//JavascriptExecutor js = (JavascriptExecutor) driver;
//		//js.executeScript("window.scrollBy(0,350)", "");
//		WebElement brokerElement = Locators.waitTillElement(driver, By.id("acBt_BrokerAccount_Create"), 20);
//		js.executeScript("arguments[0].scrollIntoView();", brokerElement);
//		Actions clickBroker = new Actions(driver);
//		clickBroker.moveToElement(brokerElement).click(brokerElement).build().perform();
//
//		Locators.waitTillElement(driver, By.id("brokerId"), 20).click();
//		//driver.findElement(ByAngular.repeater("item in brokerList").row(3)).click();
//		dropdownHandling nn = new dropdownHandling();
//		nn.select(driver, ByAngular.exactRepeater("item in brokerList"), 20, "Schwab");
//		Locators.waitTillElement(driver, By.id("accountNumber"), 20).sendKeys("123");
//		Locators.waitTillElement(driver, By.id("acBt_BrokerAccountDetail_Submit"), 20).click();
//
//		//Step 8:Add cash
//		Locators.waitTillElement(driver, By.xpath("//li[@id='prAcBt_Portfolio_Cash_Wd']//div[1]"), 20).click();
//		Locators.waitTillElement(driver, By.id("Amount_1"), 20).sendKeys("10000");
//		WebElement cashType=Locators.waitTillElement(driver, By.xpath("/html[1]/body[1]/cash-wd-details-popup[1]/div[1]/div[1]/form[1]/div[1]/div[2]/md-select[1]/md-select-value[1]/span[1]"), 20);
//		Actions clickCashType = new Actions(driver);
//		clickCashType.moveToElement(cashType).click(cashType).build().perform();
//		driver.findElement(ByAngular.repeater("(key,value) in cashFlowList").row(0)).click();
//		Locators.waitTillElement(driver, By.id("acBt_CashWdDetail_Submit"), 20).click();
//		Thread.sleep(10000);
//
//		//Step 9:Verify portfolio
//		Navigation fkk = new Navigation();
//		fkk.funNavigation("Accounts", driver);
//		QuickFilter filter = new QuickFilter(name, driver, false);
//		Thread.sleep(5000);
//		List<WebElement> gridDataElement = gridUtil.griddata(By.cssSelector("div[class='data-grid-cell ui-grid-cell-contents ']"),
//				driver, 60);
//		for (WebElement selectElement : gridDataElement) {
//			if (selectElement.getText().contains(name)) {
//				Actions cllickGrid = new Actions(driver);
//				cllickGrid.moveToElement(selectElement).doubleClick(selectElement).build().perform();
//				break;
//			}
//		}  
//
//
//
//		//Step 10:Add model
//		WebElement gdf = Locators.waitTillElement(driver, By.xpath("//em[contains(text(),'|')]"), 20);
//		gdf.click();
//		List<WebElement> eslist = gridUtil
//				.griddata(By.cssSelector("div[class='data-grid-cell ui-grid-cell-contents ']"), driver, 60);
//		for (WebElement sel : eslist) {
//			if (sel.getText().equals("Animal")) {
//				Actions cl = new Actions(driver);
//				cl.doubleClick(sel).build().perform();
//				break;
//			}
//		}
//		Locators.waitTillElement(driver, By.xpath("//em[normalize-space()='OK']"), 20).sendKeys(Keys.RETURN);
//		Thread.sleep(10000);
//		//Step 11:Set Standard/html[1]/body[1]/div[2]/div[1]/div[1]/div[2]/div[3]/portfolio-rebalance-info[1]/div[1]/div[1]/div[1]/div[1]/div[1]/md-select[2]/md-select-value[1]/span[2]
//		WebElement yh =Locators.waitTillElementAngular(driver, ByAngular.model("atomPortfolioRebalanceInfoController.rebalanceType"), 20);
//		yh.click();
//		driver.findElement(ByAngular.repeater("(key,value) in portfolioRebalanceInfoModel.rebalanceTypesDropDown").row(0)).click();
//		//Step 12:Generate order
//		Thread.sleep(5000);
//		Locators.waitTillElement(driver, By.id("prAcBt_PortfolioRebalanceInfo_GenerateOrder"), 20).click();
//		Thread.sleep(5000);
//		
//	}
//}


 
